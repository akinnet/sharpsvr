using System;
using log4net;
using sharpsvr.common;
using Xunit;

namespace sharpsvr.test.src.test.common
{
    public class TestSharpsvrConfiguration
    {
        private static ILog log = Logger.GetInstance().GetLog();

        [Fact]
        public void testReadConfig(){
            var settings = SharpSvrSettings.GetInstance();
            Assert.True(settings != null);
            Console.WriteLine("hello");
            Assert.True(settings.ByteBufferInitSize == 1);
        }
    }
}